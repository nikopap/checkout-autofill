<?php

namespace NestorsZipwise;

final class Options {
    /** @var ?self */
    private static $instance = null;
    public int $reorder_form = 0;

    public static function instance(): self {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $options = get_option(Plugin::slug);
        if (!is_array($options)) {
            return;
        }

        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
}
