<?php

namespace NestorsZipwise;

use NestorsZipwise\DB;

final class Plugin {
    const name = "Nestors Zipwise";
    const slug = "nestors-zipwise";
    const version = "0.1.0";

    /** @var self|null */
    private static $instance = null;
    private string $root_file;
    private string $basename;
    private string $base_url;
    private Settings $settings;

    private function __construct(string $root_file) {
        $this->root_file = $root_file;
        $this->basename = plugin_basename($root_file);
        $this->base_url = plugins_url("/", $root_file);
        $this->settings = new Settings();
    }

    public static function instance(string $root_file): self {
        if (!self::$instance) {
            self::$instance = new self($root_file);
        }
        return self::$instance;
    }

    public function init(): void {
        register_activation_hook($this->root_file, function () {
            DB::create_tables();
        });

        add_action("init", function () {
            load_plugin_textdomain("nestors-zipwise", false, dirname($this->basename) . "/languages");
        });

        if (is_admin()) {
            add_action("plugin_action_links_{$this->basename}", [
                $this,
                "action_links",
            ]);
            add_action("admin_menu", [$this->settings, "register_menu"]);
            add_action("admin_init", [$this->settings, "register_settings"]);
        }

        add_action("wp_enqueue_scripts", function () {
            wp_register_script(
                self::slug . "-checkout",
                $this->base_url . "dist/checkout.min.js",
                [],
                self::version,
                true,
            );

            $data = [
                "ajax_url" => admin_url("admin-ajax.php"),
            ];

            wp_add_inline_script(
                self::slug . "-checkout",
                "const CHECKOUTAUTOFILL=" . json_encode($data),
                "before",
            );

            if (is_checkout()) {
                wp_enqueue_script(self::slug . "-checkout");
            }
        });

        add_filter("woocommerce_default_address_fields", [
            $this,
            "map_default_address_fields",
        ]);
        add_action("wp_ajax_nopriv_get_postcode_autofill", [
            $this,
            "action_get_postcode_autofill",
        ]);
        add_action("wp_ajax_get_postcode_autofill", [
            $this,
            "action_get_postcode_autofill",
        ]);
        add_action("admin_post_download_postcodes", [
            $this->settings,
            "download_postcode_data",
        ]);
    }

    /**
     * @param string[] $actions
     *
     * @return string[]
     */
    public function action_links($actions) {
        $actions = array_merge(
            [
                sprintf(
                    '<a href="%s">' . __("Settings") . "</a>",
                    esc_url(
                        admin_url("options-general.php?page=" . self::slug),
                    ),
                ),
            ],
            $actions,
        );

        return $actions;
    }

    public function map_default_address_fields(array $fields): array {
        $options = Options::instance();

        if ($options->reorder_form != 1) {
            return $fields;
        }

        $fields["postcode"]["priority"] = 65;
        return $fields;
    }

    public function action_get_postcode_autofill(): void {
        $country = filter_input(
            \INPUT_POST,
            "country",
            \FILTER_SANITIZE_SPECIAL_CHARS,
        );
        $postcode = filter_input(
            \INPUT_POST,
            "postcode",
            \FILTER_SANITIZE_SPECIAL_CHARS,
        );

        if (!$country) {
            wp_send_json_error("Invalid or missing country", 406);
            return;
        }

        if (!$postcode) {
            wp_send_json_error("Invalid or missing postcode", 406);
            return;
        }

        wp_send_json(DB::get_city_and_state_by_postcode($country, $postcode));
    }
}
