<?php

namespace NestorsZipwise;

use NestorsZipwise\Plugin;

final class DB {
    public static function create_tables(): void {
        /** @var \wpdb */
        global $wpdb;

        $collate = $wpdb->get_charset_collate();
        $prefix = "{$wpdb->prefix}zipwise";

        $sql = "CREATE TABLE {$prefix}_postcodes (
            postcode_id INTEGER AUTO_INCREMENT,
            country VARCHAR(255),
            postcode VARCHAR(255),
            city VARCHAR(255),
            state VARCHAR(255) NULL,
            state_code VARCHAR(255) NULL,
            PRIMARY KEY  (postcode_id),
            UNIQUE INDEX country_postcode_idx (country, postcode)
        ) $collate;";

        /**
         * @psalm-suppress UndefinedConstant
         * @psalm-suppress UnresolvableInclude
         */
        require_once \ABSPATH . "wp-admin/includes/upgrade.php";

        dbDelta($sql);
    }

    /**
     * @return ?array{city: string, state: string, state_code: string}
     */
    public static function get_city_and_state_by_postcode(
        string $country,
        string $postcode
    ) {
        /** @var \wpdb */
        global $wpdb;

        $table = "{$wpdb->prefix}zipwise_postcodes";

        /**
         * @psalm-suppress UndefinedConstant
         * @var ?array{city: string, state: string, state_code: string}
         */
        $row = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT city, state, state_code FROM $table WHERE country = %s AND postcode = %s",
                $country,
                $postcode,
            ),
            \ARRAY_A,
        );

        return $row;
    }

    /** @return string[] */
    public static function get_all_countries() {
        /** @var \wpdb */
        global $wpdb;

        $table = "{$wpdb->prefix}zipwise_postcodes";

        /**
         * @psalm-suppress UndefinedConstant
         * @var ?mixed[]
         */
        $results = $wpdb->get_results(
            "SELECT DISTINCT country FROM $table",
            \ARRAY_A,
        );

        if (!$results) {
            return [];
        }

        return array_column($results, "country");
    }

    public static function create_postal_code(
        string $country,
        string $code,
        string $city,
        ?string $state = null,
        ?string $state_code = null
    ): void {
        /** @var \wpdb */
        global $wpdb;

        $table = "{$wpdb->prefix}zipwise_postcodes";

        $wpdb->query(
            $wpdb->prepare(
                "INSERT IGNORE INTO $table (country, postcode, city, state, state_code) VALUES (%s, %s, %s, %s, %s)",
                $country,
                $code,
                $city,
                $state,
                $state_code,
            ),
        );
    }
}
