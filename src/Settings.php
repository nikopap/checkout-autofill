<?php

namespace NestorsZipwise;

final class Settings {
    const postcode_data = [
        "GR" => [
            "country" => "Greece",
            "size" => "10.8K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/GR.zip",
        ],
        "DE" => [
            "country" => "Germany",
            "size" => "110.47K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/DE.zip",
        ],
        "ES" => [
            "country" => "Spain",
            "size" => "271.97K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/ES.zip",
        ],
        "IT" => [
            "country" => "Italy",
            "size" => "120.26K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/IT.zip",
        ],
        "BE" => [
            "country" => "Belgium",
            "size" => "20.4K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/BE.zip",
        ],
        "CY" => [
            "country" => "Cyprus",
            "size" => "7.28K",
            "url" =>
                "https://gitlab.com/nikopap/nestors-zipwise/-/raw/main/postcodes/CY.zip",
        ],
    ];

    public function register_menu(): void {
        add_options_page(
            Plugin::name,
            Plugin::name,
            "manage_options",
            Plugin::slug,
            [$this, "render_settings_page"],
        );
    }

    public function render_settings_page(): void {
        $countries = DB::get_all_countries(); ?>

        <div class="wrap">
            <h1><?php echo Plugin::name; ?></h1>
            <h2>
                <?php esc_html_e("Install Postal Code Data", "nestors-zipwise"); ?>
            </h2>
            <p>
                <?php esc_html_e(
                    "You can choose which for which country that you want to have autofilled by using the actions bellow. The postal code data will be downloaded and saved to your website's database.",
                    "nestors-zipwise",
                ); ?>
            </p>
            <table class="wp-list-table widefat fixed striped table-view-list">
                <tr>
                    <th><?php esc_html_e("Country"); ?></th>
                    <th><?php esc_html_e("Country Code", "nestors-zipwise"); ?></th>
                    <th><?php esc_html_e("Size in bytes", "nestors-zipwise"); ?></th>
                    <th></th>
                </tr>
                <?php foreach (self::postcode_data as $code => $data): ?>
                <tr>
                    <td><?php echo esc_html($data["country"]); ?> </td>
                    <td><?php echo esc_html($code); ?> </td>
                    <td><?php echo esc_html($data["size"]); ?> </td>
                    <td>
                        <form action="admin-post.php" method="post">
                            <input type="hidden" name="action" value="download_postcodes" />
                            <input type="hidden" name="country" value="<?php echo esc_attr(
                                $code,
                            ); ?>" />
                            <button class="button button-secondary" type="submit">
                                <?php echo in_array($code, $countries)
                                    ? esc_html__("Re-install")
                                    : esc_html__("Install"); ?>
                            </button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <p>
                <?php esc_html_e(
                    "The Data is provided by www.geonames.org",
                    "nestors-zipwise",
                ); ?>
                <br/>
                <?php esc_html_e(
                    'NOTICE: The Data is provided "as is" without warranty or any representation of accuracy, timeliness or completeness.',
                    "nestors-zipwise",
                ); ?>
            </p>
            <h2><?php _e("Settings"); ?></h2>
            <form action="options.php" method="post">
                <?php
                settings_fields(Plugin::slug);
                do_settings_sections(Plugin::slug);
                submit_button();?>
            </form>
        </div>
        <?php
    }

    public function register_settings(): void {
        $options = Options::instance();

        register_setting(Plugin::slug, Plugin::slug);

        add_settings_section(
            Plugin::slug . "_section",
            "",
            function () {},
            Plugin::slug,
        );

        add_settings_field(
            "reorder_form",
            esc_html__("Postal Code field in Checkout form", "nestors-zipwise"),
            function () use ($options) {
                ?>
                <label>
                    <input type='checkbox' name='nestors-zipwise[reorder_form]' value='1' <?php checked(
                        $options->reorder_form,
                        1,
                    ); ?> />
                    <?php esc_html_e(
                        "Set the postal code input field to a higher priority (recommended)",
                        "nestors-zipwise",
                    ); ?>
                </label>
                <?php
            },
            Plugin::slug,
            Plugin::slug . "_section",
        );
    }

    /** @return never */
    public function download_postcode_data() {
        $country = filter_input(
            \INPUT_POST,
            "country",
            \FILTER_SANITIZE_SPECIAL_CHARS,
        );

        if (!$country) {
            wp_die("Invalid form submission (missing country field)");
        }

        if (!array_key_exists($country, self::postcode_data)) {
            wp_die("Unknown country code $country");
        }

        $url = self::postcode_data[$country]["url"];
        $result = $this->download_and_extract_data($url, $country);
        if (is_wp_error($result)) {
            wp_die($result);
        }

        wp_redirect(admin_url("options-general.php?page=" . Plugin::slug));
        exit();
    }

    /** @return true|\WP_Error */
    private function download_and_extract_data(
        string $url,
        string $country_code
    ) {
        WP_Filesystem();
        $tempdir = get_temp_dir();

        $filename = download_url($url);
        if (is_wp_error($filename)) {
            return $filename;
        }

        $unziped = unzip_file($filename, $tempdir);
        if (is_wp_error($unziped)) {
            return $unziped;
        }

        $file = fopen($tempdir . "{$country_code}.txt", "r");
        if (!$file) {
            return new \WP_Error(
                "read_file",
                "Could not open file {$country_code}.txt",
            );
        }

        foreach ($this->read_file($file) as $row) {
            [$postcode, $city, $state, $state_code] = [
                $row[1],
                $row[2],
                $row[3],
                $row[4],
            ];

            DB::create_postal_code(
                $country_code,
                $postcode,
                $city,
                $state,
                $state_code,
            );
        }

        unlink($filename);

        return true;
    }

    /**
     * @param resource $file
     *
     * @return \Generator<int, array<string>, mixed, void>
     */
    private function read_file(&$file) {
        while (($row = fgetcsv($file, 0, "\t")) !== false) {
            yield $row;
        }

        return;
    }
}
