declare var CHECKOUTAUTOFILL: {
  ajax_url: string;
};

type PostcodeData = {
  city: string;
  state: string | null;
  state_code: string | null;
};

async function getPostcodeData(
  country: string,
  postcode: string,
): Promise<PostcodeData | null> {
  const res = await fetch(CHECKOUTAUTOFILL.ajax_url, {
    method: "post",
    body: new URLSearchParams({
      action: "get_postcode_autofill",
      country,
      postcode,
    }),
  });

  if (!res.ok) {
    return null;
  }

  return await res.json();
}

function blockUi() {
  /* @ts-ignore */
  jQuery("#customer_details").block({
    message: null,
    overlayCSS: {
      background: "#fff",
      opacity: 0.6,
    },
  });
}

function unblockUi() {
  /* @ts-ignore */
  jQuery("#customer_details").unblock();
}

document.addEventListener("DOMContentLoaded", () => {
  for (const prefix of ["billing_", "shipping_"]) {
    const countryField = document.getElementById(
      `${prefix}country`,
    ) as HTMLSelectElement | null;

    const postcodeField = document.getElementById(
      `${prefix}postcode`,
    ) as HTMLInputElement | null;

    const cityField = document.getElementById(
      `${prefix}city`,
    ) as HTMLInputElement | null;

    const stateField = document.getElementById(`${prefix}state`) as
      | HTMLSelectElement
      | HTMLInputElement
      | null;

    postcodeField?.addEventListener("change", () => {
      const postcodeVal = postcodeField.value.replace(/\s/g, "");
      const countryVal = countryField?.value;

      if (!postcodeVal || !countryVal) {
        return;
      }

      blockUi();
      getPostcodeData(countryVal, postcodeVal)
        .then((data) => {
          if (!data) {
            return;
          }

          if (cityField) {
            jQuery(cityField).val(data.city);
          }

          if (stateField) {
            const inputType = stateField.getAttribute("type");
            if (inputType == "text" && data.state != null) {
              jQuery(stateField).val(data.state).trigger("change");
            } else if (stateField.nodeName == "SELECT" && data.state_code != null) {
              jQuery(stateField).val(data.state_code).trigger("change");
            }
          }

          // TODO: Trigger update_checkout
          // jQuery(document.body).trigger("update_checkout");
        })
        .finally(() => {
          unblockUi();
        });
    });
  }
});
