<?php

/*
 * Plugin Name: Nestors Zipwise - for WooCommerce
 * Description: Autofills city and state when a customer fills their zipcode.
 * Version: 0.1.0
 * Author: Nikos Papadakis, WebNestors
 * Author URI: https://webnestors.com
 * Text Domain: nestors-zipwise
 * Domain Path: /languages
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

// If this file is called directly, abort.
if (!defined("WPINC")) {
    die();
}

/** @psalm-suppress UnresolvableInclude */
require plugin_dir_path(__FILE__) . "vendor/autoload.php";

NestorsZipwise\Plugin::instance(__FILE__)->init();
